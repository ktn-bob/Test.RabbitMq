﻿using System;
using System.Threading.Tasks;
using NServiceBus;
using NServiceBus.Features;

namespace Sales
{
    class Program
    {
        static async Task Main()
        {
            Console.Title = "Sales";

            var endpointConfiguration = new EndpointConfiguration("Sales");

            //endpointConfiguration.UseTransport<LearningTransport>();

            //var transport = endpointConfiguration.UseTransport<AzureServiceBusTransport>();
            //transport.ConnectionString("Endpoint=sb://sb-ac-eu1-platobus-dvlp.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=KoThv4McZXuey/kOyoAAnRwHk7h/aoWsxPWSTqaNac0=")
            //    .UseForwardingTopology();


            endpointConfiguration.DisableFeature<TimeoutManager>();
            var transport = endpointConfiguration.UseTransport<RabbitMQTransport>();
            transport.ConnectionString("host=s-be-ki-brx-dev;username=brix;password=brx123");

            var endpointInstance = await Endpoint.Start(endpointConfiguration)
                .ConfigureAwait(false);

            Console.WriteLine("Press Enter to exit.");
            Console.ReadLine();

            await endpointInstance.Stop()
                .ConfigureAwait(false);
        }
    }
}